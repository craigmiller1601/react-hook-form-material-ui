import { createContext, useContext } from 'react';
import {
    Controller,
    type ControllerFieldState,
    type FieldValues
} from 'react-hook-form';
import type { DefaultProps, Rules } from '../types/form.js';
import {
    type DateView,
    DatePicker as MuiDatePicker,
    DesktopDatePicker as MuiDesktopDatePicker
} from '@mui/x-date-pickers';
import { TextField as MuiTextField } from '@mui/material';
import { isValid, parse } from 'date-fns';
import { useControlId } from '../utils/useControlId.js';

const MIN_DATE = parse('1970-01-01', 'yyyy-MM-dd', new Date());
const MAX_DATE = parse('2100-01-01', 'yyyy-MM-dd', new Date());

const validate = (value?: Date): string | undefined =>
    !value || isValid(value) ? undefined : 'Must be valid date';

type Props<F extends FieldValues> = DefaultProps<F> &
    Readonly<{
        views?: ReadonlyArray<DateView>;
        minDate?: Date;
        maxDate?: Date;
    }>;

type DatePickerFieldContextValue = Readonly<{
    id?: string;
    testId?: string;
    onValueHasChanged?: () => void;
    fieldState: ControllerFieldState;
}>;
const DatePickerFieldContext = createContext<DatePickerFieldContextValue>({
    fieldState: {
        invalid: false,
        isDirty: false,
        isTouched: false,
        isValidating: false
    }
});
const DatePickerField = (props: Parameters<typeof MuiTextField>[0]) => {
    const context = useContext(DatePickerFieldContext);
    return (
        <MuiTextField
            {...props}
            id={context.id}
            inputProps={{
                ...props.inputProps,
                'data-testid': context.testId
            }}
            onBlur={(event) => {
                props.onBlur?.(event);
                context.onValueHasChanged?.();
            }}
            error={!!context.fieldState.error}
            helperText={context.fieldState.error?.message ?? ''}
        />
    );
};

const chooseMuiDatePicker = (): typeof MuiDatePicker => {
    if (window.Cypress) {
        return MuiDesktopDatePicker;
    }
    return MuiDatePicker;
};

export const DatePicker = <F extends FieldValues>(props: Props<F>) => {
    const rules: Rules<F> = {
        ...(props.rules ?? {}),
        validate
    };
    const { inputId } = useControlId(props.id);

    const ChosenMuiDatePicker = chooseMuiDatePicker();

    return (
        <Controller
            name={props.name}
            control={props.control}
            rules={rules}
            render={({ field, fieldState }) => {
                const datePickerFieldProps: DatePickerFieldContextValue = {
                    id: inputId,
                    fieldState,
                    onValueHasChanged: props.onValueHasChanged,
                    testId: props.testId
                };

                return (
                    <DatePickerFieldContext.Provider
                        value={datePickerFieldProps}
                    >
                        <ChosenMuiDatePicker
                            {...field}
                            className={props.className}
                            views={props.views}
                            onAccept={() => props.onValueHasChanged?.()}
                            label={props.label}
                            disabled={props.disabled}
                            slots={{
                                textField: DatePickerField
                            }}
                            minDate={props.minDate ?? MIN_DATE}
                            maxDate={props.maxDate ?? MAX_DATE}
                        />
                    </DatePickerFieldContext.Provider>
                );
            }}
        />
    );
};
